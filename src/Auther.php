<?php
use Enqueue\Dsn\Dsn;
use Medoo\Medoo;

class Auther{
    function __construct(){
        global $_ENV;
        global $_COOKIE;

        $this->DSN = Dsn::parseFirst($_ENV['DB_AUTH_DSN']);
        $this->Database = new Medoo([
            'database_type' => $this->DSN->getSchemeProtocol(),
            'database_name' => 'Auther',
            'server' => $this->DSN->getHost(),
            'username' => $this->DSN->getUser(),
            'password' => $this->DSN->getPassword(),
            'port' => $this->DSN->getPort()
        ]);

        if(isset($_COOKIE['Session'])){
            $Session = $this->Database->select('Sessions', '*', ['Token' => $_COOKIE['Session'], 'LIMIT' => 1]);
            if(count($Session) == 1){
                $Session = $Session[0];
                if($Session['Expired'] > time()){
                    $User = $this->Database->select('Users', '*', ['id' => $Session['User'], 'LIMIT' => 1]);
                    if(count($User) == 1){
                        $this->User = $User[0];
                        setcookie('Session', $_COOKIE['Session'], time() + 300, '/', $_ENV['COOKIEDOMAIN']);
                        $this->Database->update('Sessions', ['Updated' => time()], ['id' => $Session['id'], 'LIMIT' => 1]);
                        $this->Database->update('Sessions', ['Expired' => time() + 300], ['id' => $Session['id'], 'LIMIT' => 1]);
                    }else{
                        //Clear Cookies
                        $this->User = false;
                    }
                }else{
                    //Clear Cookies
                    $this->User = false;
                }
            }else{
                //Clear Cookies
                $this->User = false;
            }
        }else{
            $this->User = false;
        }
    }

    function Database(){
        return $this->Database;
    }

    function User(){
        return $this->User;
    }
}